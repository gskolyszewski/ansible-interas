#!/bin/bash

scp -r ../ansible-interas/ gns3@$1:~
ssh gns3@$1 "sudo cp -r ~/ansible-interas $2/root/"